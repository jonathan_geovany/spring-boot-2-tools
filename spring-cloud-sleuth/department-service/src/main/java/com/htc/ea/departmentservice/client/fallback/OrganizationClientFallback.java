package com.htc.ea.departmentservice.client.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.htc.ea.departmentservice.client.OrganizationClient;
import com.htc.ea.departmentservice.util.AppConst;

@Component
public class OrganizationClientFallback implements OrganizationClient{

	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Override
	public ResponseEntity<Boolean> organizationExist(Long id) {
		log.error("Entrada a metodo de respaldo: organizationExist");
		HttpHeaders headers = new HttpHeaders();
		headers.add(AppConst.MSG_ERROR, "No se pudo conectar al servicio: Organization");
		return new ResponseEntity<>(null,headers,HttpStatus.SERVICE_UNAVAILABLE);
	}

}
