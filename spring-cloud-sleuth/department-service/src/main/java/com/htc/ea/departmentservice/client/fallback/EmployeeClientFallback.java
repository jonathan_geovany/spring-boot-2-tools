package com.htc.ea.departmentservice.client.fallback;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.htc.ea.departmentservice.client.EmployeeClient;
import com.htc.ea.departmentservice.model.Employee;
import com.htc.ea.departmentservice.util.AppConst;

@Component
public class EmployeeClientFallback implements EmployeeClient{

	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Override
	public ResponseEntity<List<Employee>> findEmployeesByIdDepartment(Long id) {
		log.error("Entrada a metodo de respaldo: findEmployeesByIdDepartment");
		HttpHeaders headers = new HttpHeaders();
		headers.add(AppConst.MSG_ERROR, "No se pudo conectar al servicio: Employee");
		return new ResponseEntity<>(null,headers,HttpStatus.SERVICE_UNAVAILABLE);
	}
}
