package com.htc.ea.departmentservice.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.htc.ea.departmentservice.client.fallback.EmployeeClientFallback;
import com.htc.ea.departmentservice.model.Employee;

//@FeignClient(name = "employee-service" , fallback = EmployeeClientFallback.class)
@FeignClient(name = "employee-service")
public interface EmployeeClient {

	/**
	 * consulta al microservicio employee
	 * retorna lista de empleados que esten bajo el departamento enviado
	 * contiene una funcion fallback en el caso de que no se pueda conectar al microservicio
	 * @param id departamento
	 * @return response entity con headers y lista de employee en el body
	 */
	@GetMapping("/department/{id}")
	ResponseEntity<List<Employee>> findEmployeesByIdDepartment(@PathVariable("id") Long id);
}
