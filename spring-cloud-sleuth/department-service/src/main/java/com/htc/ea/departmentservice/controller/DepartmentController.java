package com.htc.ea.departmentservice.controller;


import java.util.List;

import javax.validation.Valid;

import org.dozer.Mapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.ContinueSpan;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.htc.ea.departmentservice.dto.DepartmentRequest;
import com.htc.ea.departmentservice.model.Department;
import com.htc.ea.departmentservice.service.DepartmentService;

import brave.Span;
import brave.Tracer;


@RestController
public class DepartmentController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentController.class);
	
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private Mapper mapper;
	
	@Autowired
	Tracer tracer;
	
	/**
	 * peticion tipo post de guardado de departamento
	 * llama a department service para realizar operacion de guardado
	 * @param request dto de entrada que se mapea a Deparment
	 * @return response entity con cabeceras de mensajes, department en el body, y http status
	 */
	@PostMapping("/")
	public ResponseEntity<Department> save(@Valid @RequestBody DepartmentRequest request) {
		Span span = tracer.nextSpan().name("GUARDAR EMPLEADOS");
		
		ResponseEntity<Department> re = departmentService.save(mapper.map(request, Department.class));
		
		span.tag("guarda", re.getBody().toString());
		span.finish();
		
		
		LOGGER.info("Department : {}", request);
		return departmentService.save(mapper.map(request, Department.class));//mapeo la entrada DeparmentRequest y obtengo sus valores y los paso al objeto Department
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Department> findById(@PathVariable("id") Long id) {
		LOGGER.info("Department found: id={}", id);
		return departmentService.findById(id);
	}
	
	@GetMapping("/")
	public ResponseEntity<List<Department>> findAll() {
		LOGGER.info("Departments findAll");
		return departmentService.findAll();
	}
	
	@GetMapping("/organization/{id}")
	public ResponseEntity<List<Department>> findByOrganization(@PathVariable("id") Long id) {
		Span span = tracer.nextSpan().name("Encuentra organizationconEmpleados");
		span.tag("muestra", "requestID");
		span.finish();
		LOGGER.info("Department find: id={}", id);
		return departmentService.findAllByIdOrganization(id);
	}
	
	@GetMapping("/organization/{id}/with-employees")
	public ResponseEntity<List<Department>> findAllByIdOrganizationWithEmployees(@PathVariable("id") Long id) {
		
		Span span = tracer.nextSpan().name("Encuentra organizationconEmpleados");
		span.tag("muestra", "requestID");
		span.finish();
		
		LOGGER.info("Department find: id={}", id);
		return departmentService.findAllByIdOrganizationWithEmployees(id);
	}
	
}
