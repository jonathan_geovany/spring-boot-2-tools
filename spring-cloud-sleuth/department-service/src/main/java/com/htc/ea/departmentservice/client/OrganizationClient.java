package com.htc.ea.departmentservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.htc.ea.departmentservice.client.fallback.OrganizationClientFallback;

//@FeignClient(name = "organization-service" , fallback = OrganizationClientFallback.class)
@FeignClient(name = "organization-service")
public interface OrganizationClient {
	/**
	 * consulta a microservicio organization
	 * retorna un valor boolean en el body donde: es true si se encontro, false si no
	 * contiene una funcion fallback en el caso de que no se pueda conectar al microservicio
	 * @param id organization
	 * @return response entity con headers y valor boolean en el body
	 */
	@GetMapping("/organization/exist/{id}")
	public ResponseEntity<Boolean> organizationExist(@PathVariable("id") Long id);
}
