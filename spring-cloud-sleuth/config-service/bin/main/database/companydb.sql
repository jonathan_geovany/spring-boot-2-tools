
--CREATE DATABASE companydb;


CREATE TABLE organization(
	id serial PRIMARY KEY NOT NULL,
	name varchar(30) NOT NULL,
	address varchar(50) NOT NULL
);

CREATE TABLE department(
	id serial PRIMARY KEY NOT NULL,
	id_organization int REFERENCES organization (id) NOT NULL,
	name varchar(50) NOT NULL
);

CREATE TABLE employee(
	id serial PRIMARY KEY NOT NULL,
	id_organization int REFERENCES organization (id) NOT NULL,
	id_department int REFERENCES department (id) NOT NULL,
	name varchar(50) NOT NULL,
	age int NOT NULL,
	position varchar(30) NOT NULL
);

INSERT INTO public.organization
("name", address)
VALUES('organization1', 'Santa Ana'),('organization2', 'San Miguel'),('organization3', 'Sonsonate');

select * from organization;

INSERT INTO public.department
(id_organization, "name")
VALUES(1, 'QA'), (2, 'QA'),(3, 'QA');

select * from department;

INSERT INTO public.employee
(id_organization, id_department, "name", age, "position")
VALUES(1, 1, 'Hector', 21, 'Contador'),(2, 2, 'Maria', 25, 'Contador'),(3, 3, 'Luis', 32, 'Contador');

select * from employee;