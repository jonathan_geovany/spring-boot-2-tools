package com.htc.ea.employeeservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.htc.ea.employeeservice.client.fallback.DepartmentClientFallback;
import com.htc.ea.employeeservice.model.Department;

//@FeignClient(name = "department-service" ,fallback = DepartmentClientFallback.class)
@FeignClient(name = "department-service")
public interface DepartmentClient {
	
	@GetMapping("/{id}")
	public ResponseEntity<Department> findById(@PathVariable("id") Long id);
}
