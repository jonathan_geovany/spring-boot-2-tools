package com.htc.ea.employeeservice.config;


import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)
          .apiInfo(apiInfo())
          .select()                                  
          .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
          .paths(PathSelectors.any())       
          .build();                                           
    }
	private ApiInfo apiInfo() {
	    return new ApiInfo(
	      "Employee-Service", 
	      "Servicio de control de empleados asignados a un departamento dentro de una organizacion.", 
	      "API SERVICE", 
	      "Terms of service", 
	      new Contact("HTC", "http://www.hightech-corp.com/", ""), 
	      "License of API", "API license URL", Collections.emptyList());
	}
}
