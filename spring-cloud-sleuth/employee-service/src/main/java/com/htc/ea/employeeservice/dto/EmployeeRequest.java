package com.htc.ea.employeeservice.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

public class EmployeeRequest {
	
	@NotNull
	private Long idDepartment;
	@NotNull
	@Length(min=1,max=30)
	private String name;
	@NotNull
	@Positive
	private int age;
	@NotNull
	@Length(min=1,max=30)
	private String position;
	
	
	
	
	public Long getIdDepartment() {
		return idDepartment;
	}
	public void setIdDepartment(Long idDepartment) {
		this.idDepartment = idDepartment;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmployeeRequest [departmentId=");
		builder.append(idDepartment);
		builder.append(", name=");
		builder.append(name);
		builder.append(", age=");
		builder.append(age);
		builder.append(", position=");
		builder.append(position);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
