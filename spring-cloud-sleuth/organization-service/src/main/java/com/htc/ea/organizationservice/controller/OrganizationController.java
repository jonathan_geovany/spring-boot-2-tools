package com.htc.ea.organizationservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.auditing.CurrentDateTimeProvider;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.htc.ea.organizationservice.dto.OrganizationRequest;
import com.htc.ea.organizationservice.model.Organization;
import com.htc.ea.organizationservice.service.OrganizationService;
import com.rabbitmq.client.impl.AMQImpl.Access.Request;

import brave.Span;
import brave.Tracer;
import brave.Tracing;


@RestController
public class OrganizationController {
	
	private static final String ORGANIZATION_FOUND_ID = "Organization found: id={}";

	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private OrganizationService organizationService;
	@Autowired
	private Mapper mapper;
	
	@Autowired
	Tracer tracer;
	
	@Autowired
	Tracing tracing;

	
	@PostMapping("/")
	public ResponseEntity<Organization> save(@Valid @RequestBody OrganizationRequest request) {		
		Span span = tracer.nextSpan().name("GUARDAR ORGANIZACION");
		span.start();
		
		ResponseEntity<Organization> response = organizationService.save(mapper.map(request, Organization.class));
		
		/*ObjectMapper objectMapper = new ObjectMapper();
		try {
			span.tag("Response",objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(response.getBody()));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		//rabbitmq.send(response);
		
		log.info("Organization : {}", request);
		
		
		span.finish();
		
		//return organizationService.save(mapper.map(request, Organization.class));
		return response;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Organization> findById(@PathVariable("id") Long id) {
		log.info(ORGANIZATION_FOUND_ID, id);
		return organizationService.findById(id);
	}
	
	@GetMapping("/")
	public ResponseEntity<List<Organization>> findAll() {
		log.info("Organization findAll");
		return organizationService.findAll();
	}
	
	@GetMapping("/{id}/with-departments")
	public ResponseEntity<Organization> findByIdWithDepartments(@PathVariable("id") Long id) {
		log.info(ORGANIZATION_FOUND_ID, id);
		return organizationService.findByIdWithDepartments(id);
	}
	
	@GetMapping("/{id}/with-departments-and-employees")
	public ResponseEntity<Organization> findByIdWithDepartmentsAndEmployees(@PathVariable("id") Long id) {
		log.info(ORGANIZATION_FOUND_ID, id);
		return organizationService.findByIdWithDepartmentsWithEmployees(id);
	}
	
	@GetMapping("/{id}/with-employees")
	public ResponseEntity<Organization> findByIdWithEmployees(@PathVariable("id") Long id) {
		log.info(ORGANIZATION_FOUND_ID, id);
		return organizationService.findByIdWithEmployees(id);
	}
}
