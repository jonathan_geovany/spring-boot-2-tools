package com.htc.ea.organizationservice.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.htc.ea.organizationservice.client.fallback.EmployeeClientFallback;
import com.htc.ea.organizationservice.model.Employee;


//@FeignClient(name = "employee-service" ,fallback = EmployeeClientFallback.class)
@FeignClient(name = "employee-service")
public interface EmployeeClient {
	@GetMapping("/organization/{id}")
	public ResponseEntity<List<Employee>> findAllEmployeesByIdOrganization(@PathVariable("id") Long id); 
}
