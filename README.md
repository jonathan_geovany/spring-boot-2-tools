# spring-boot-2-tools

Herramientas de spring boot:

- Spring cloud Gateway - punto de acceso centralizado a las APIs  - Jonathan
- Spring cloud Sleuth - rastreo de peticiones mediante los servicios - Manuel
- Netflix OSS Conductor - Orquestador de servicios - Esteban