package com.htc.ea.organizationservice.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.htc.ea.organizationservice.client.fallback.DepartmentClientFallback;
import com.htc.ea.organizationservice.model.Department;


@FeignClient(name = "department-service" ,fallback = DepartmentClientFallback.class)
public interface DepartmentClient {
	
	@GetMapping("/organization/{id}")
	public ResponseEntity<List<Department>> findAllDepartmentsByIdOrganization(@PathVariable("id") Long id);
	
	@GetMapping("/organization/{id}/with-employees")
	public ResponseEntity<List<Department>> findAllDepartmentsByIdOrganizationWithEmployees(@PathVariable("id") Long id);
}
