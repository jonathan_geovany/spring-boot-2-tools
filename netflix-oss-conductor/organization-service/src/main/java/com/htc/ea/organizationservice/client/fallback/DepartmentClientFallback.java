package com.htc.ea.organizationservice.client.fallback;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.htc.ea.organizationservice.client.DepartmentClient;
import com.htc.ea.organizationservice.model.Department;
import com.htc.ea.organizationservice.util.AppConst;



@Component
public class DepartmentClientFallback implements DepartmentClient{

	private Logger log = LoggerFactory.getLogger(getClass());

	@Override
	public ResponseEntity<List<Department>> findAllDepartmentsByIdOrganization(Long id) {
		log.error("Entrada a metodo de respaldo: findAllDepartmentsByIdOrganization");
		HttpHeaders headers = new HttpHeaders();
		headers.add(AppConst.MSG_ERROR, "No se pudo conectar al servicio: Department");
		return new ResponseEntity<>(null,headers,HttpStatus.SERVICE_UNAVAILABLE);
	}

	@Override
	public ResponseEntity<List<Department>> findAllDepartmentsByIdOrganizationWithEmployees(Long id) {
		log.error("Entrada a metodo de respaldo: findAllDepartmentsByIdOrganizationWithEmployees");
		HttpHeaders headers = new HttpHeaders();
		headers.add(AppConst.MSG_ERROR, "No se pudo conectar al servicio: Department");
		return new ResponseEntity<>(null,headers,HttpStatus.SERVICE_UNAVAILABLE);
	}
}
