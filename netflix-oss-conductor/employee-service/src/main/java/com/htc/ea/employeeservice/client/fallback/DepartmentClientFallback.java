package com.htc.ea.employeeservice.client.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.htc.ea.employeeservice.util.AppConst;
import com.htc.ea.employeeservice.client.DepartmentClient;
import com.htc.ea.employeeservice.model.Department;

@Component
public class DepartmentClientFallback implements DepartmentClient{

	private Logger log = LoggerFactory.getLogger(getClass());
	

	@Override
	public ResponseEntity<Department> findById(Long id) {
		log.error("Entrada a metodo de respaldo: findById");
		HttpHeaders headers = new HttpHeaders();
		headers.add(AppConst.MSG_ERROR, "No se pudo conectar al servicio: Department");
		return new ResponseEntity<>(null,headers,HttpStatus.SERVICE_UNAVAILABLE);
	}

}
