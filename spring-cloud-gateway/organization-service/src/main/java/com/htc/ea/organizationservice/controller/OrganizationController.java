package com.htc.ea.organizationservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.htc.ea.organizationservice.dto.OrganizationRequest;
import com.htc.ea.organizationservice.model.Organization;
import com.htc.ea.organizationservice.service.OrganizationService;


@RestController
public class OrganizationController {
	
	private static final String ORGANIZATION_FOUND_ID = "Organization found: id={}";

	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private OrganizationService organizationService;
	@Autowired
	private Mapper mapper;
	
	@PostMapping(value="/",produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Organization> save(@Valid @RequestBody OrganizationRequest request) {
		log.info("Organization : {}", request);
		return organizationService.save(mapper.map(request, Organization.class));
	}
	
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Organization> findById(@PathVariable("id") Long id) {
		log.info(ORGANIZATION_FOUND_ID, id);
		return organizationService.findById(id);
	}
	
	@GetMapping(value="/",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Organization>> findAll() {
		log.info("Organization findAll");
		return organizationService.findAll();
	}
	
	@GetMapping(value="/{id}/with-departments",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Organization> findByIdWithDepartments(@PathVariable("id") Long id) {
		log.info(ORGANIZATION_FOUND_ID, id);
		return organizationService.findByIdWithDepartments(id);
	}
	
	@GetMapping(value="/{id}/with-departments-and-employees",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Organization> findByIdWithDepartmentsAndEmployees(@PathVariable("id") Long id) {
		log.info(ORGANIZATION_FOUND_ID, id);
		return organizationService.findByIdWithDepartmentsWithEmployees(id);
	}
	
	@GetMapping(value="/{id}/with-employees",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Organization> findByIdWithEmployees(@PathVariable("id") Long id) {
		log.info(ORGANIZATION_FOUND_ID, id);
		return organizationService.findByIdWithEmployees(id);
	}
	
	@GetMapping(value="/organization/exist/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> organizationExist(@PathVariable("id") Long id){
		return organizationService.exist(id);
	}
}
