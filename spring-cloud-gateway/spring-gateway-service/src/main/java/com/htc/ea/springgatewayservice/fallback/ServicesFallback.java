package com.htc.ea.springgatewayservice.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServicesFallback {

	private Logger log = LoggerFactory.getLogger(getClass());
	private static final String ERROR = "Error";
	
	@GetMapping("/departmentFallback")
	public ResponseEntity<Object> departmentFallback(){
		log.error("Entrada al metodo de error de: department");
		HttpHeaders headers = new HttpHeaders();
		headers.add(ERROR, "No se pudo acceder al servicio de departamento");
		return new ResponseEntity<>(null,headers,HttpStatus.GATEWAY_TIMEOUT);
	}
	@GetMapping("/employeeFallback")
	public ResponseEntity<Object> employeeFallback(){
		log.error("Entrada al metodo de error de: employee");
		HttpHeaders headers = new HttpHeaders();
		headers.add(ERROR, "No se pudo acceder al servicio de empleado");
		return new ResponseEntity<>(null,headers,HttpStatus.GATEWAY_TIMEOUT);
	}
	@GetMapping("/organizationFallback")
	public ResponseEntity<Object> organizationFallback(){
		log.error("Entrada al metodo de error de: organization");
		HttpHeaders headers = new HttpHeaders();
		headers.add(ERROR, "No se pudo acceder al servicio de organizacion");
		return new ResponseEntity<>(null,headers,HttpStatus.GATEWAY_TIMEOUT);
	}
}
