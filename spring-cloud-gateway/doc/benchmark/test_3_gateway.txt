This is ApacheBench, Version 2.3 <$Revision: 1807734 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient).....done


Server Software:        
Server Hostname:        localhost
Server Port:            8060

Document Path:          /department/
Document Length:        692 bytes

Concurrency Level:      5
Time taken for tests:   0.571 seconds
Complete requests:      100
Failed requests:        5
   (Connect: 0, Receive: 0, Length: 5, Exceptions: 0)
Total transferred:      88635 bytes
HTML transferred:       69235 bytes
Requests per second:    175.17 [#/sec] (mean)
Time per request:       28.544 [ms] (mean)
Time per request:       5.709 [ms] (mean, across all concurrent requests)
Transfer rate:          151.62 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       1
Processing:     6   28  16.5     23      89
Waiting:        6   27  16.6     22      89
Total:          6   28  16.5     23      89

Percentage of the requests served within a certain time (ms)
  50%     23
  66%     30
  75%     34
  80%     39
  90%     51
  95%     67
  98%     86
  99%     89
 100%     89 (longest request)
