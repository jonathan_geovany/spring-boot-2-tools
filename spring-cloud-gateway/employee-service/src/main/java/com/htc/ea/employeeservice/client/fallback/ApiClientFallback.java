package com.htc.ea.employeeservice.client.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.htc.ea.employeeservice.client.ApiClient;
import com.htc.ea.employeeservice.model.Department;
import com.htc.ea.employeeservice.util.AppConst;

import feign.hystrix.FallbackFactory;

@Component
public class ApiClientFallback implements FallbackFactory<ApiClient>{

	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Override
	public ApiClient create(Throwable cause) {
		
		log.error("Feign error: {}",cause);
		
		return new ApiClient() {
			
			@Override
			public ResponseEntity<Department> findDepartmentById(Long id) {
				log.error("Entrada a metodo de respaldo: findById");
				HttpHeaders headers = new HttpHeaders();
				headers.add(AppConst.MSG_ERROR, "No se pudo conectar al servicio: Department");
				return new ResponseEntity<>(null,headers,HttpStatus.SERVICE_UNAVAILABLE);
			}
		};
	}

}
