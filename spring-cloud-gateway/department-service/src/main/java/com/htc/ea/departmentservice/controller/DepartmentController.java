package com.htc.ea.departmentservice.controller;


import java.util.List;

import javax.validation.Valid;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.htc.ea.departmentservice.dto.DepartmentRequest;
import com.htc.ea.departmentservice.model.Department;
import com.htc.ea.departmentservice.service.DepartmentService;

@RestController
public class DepartmentController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentController.class);
	
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private Mapper mapper;
	
	/**
	 * peticion tipo post de guardado de departamento
	 * llama a department service para realizar operacion de guardado
	 * @param request dto de entrada que se mapea a Deparment
	 * @return response entity con cabeceras de mensajes, department en el body, y http status
	 */
	@PostMapping(value="/",produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Department> save(@Valid @RequestBody DepartmentRequest request) {
		LOGGER.info("Department : {}", request);
		return departmentService.save(mapper.map(request, Department.class));//mapeo la entrada DeparmentRequest y obtengo sus valores y los paso al objeto Department
	}
	
	/**
	 * peticion get que obtiene un departamento en base al id
	 * @param id departamento
	 * @return response entity con cabeceras de mensajes, y department en el body si se encontro
	 */
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Department> findById(@PathVariable("id") Long id) {
		LOGGER.info("Department found: id={}", id);
		return departmentService.findById(id);
	}
	
	/**
	 * peticion get que obtiene todos los departamentos
	 * @return response entity con cabeceras de mensajes, y department en el body
	 */
	@GetMapping(value="/",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Department>> findAll() {
		LOGGER.info("Departments findAll");
		return departmentService.findAll();
	}
	
	/**
	 * peticion get que obtiene todos los departamentos que esten bajo el id de la organizacion
	 * @param id organization
	 * @return response entity con cabeceras de mensajes, y lista de department en el body
	 */
	@GetMapping(value="/organizations/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Department>> findByOrganization(@PathVariable("id") Long id) {
		LOGGER.info("Department find: id={}", id);
		return departmentService.findAllByIdOrganization(id);
	}
	
	/**
	 * peticion get que obtiene todos los departamentos que esten bajo un id de organizacion
	 * ademas obtiene los empleados que esten en cada departamento
	 * @param id organization
	 * @return response entity con cabeceras de mensajes, y lista de department con employees en el body
	 */
	@GetMapping(value="/organizations/{id}/employees/",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Department>> findAllByIdOrganizationWithEmployees(@PathVariable("id") Long id) {
		LOGGER.info("Department find: id={}", id);
		return departmentService.findAllByIdOrganizationWithEmployees(id);
	}
	
}
