package com.htc.ea.departmentservice.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.htc.ea.departmentservice.client.ApiClient;
import com.htc.ea.departmentservice.model.Department;
import com.htc.ea.departmentservice.model.Employee;
import com.htc.ea.departmentservice.repository.DepartmentRepository;
import com.htc.ea.departmentservice.util.AppConst;

@Service
public class DepartmentService {
	
	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private ApiClient apiClient;
	
	/**
	 * funcion de guardado de departamento
	 * evalua si el id de la organization es valido
	 * consultado al servicio organization
	 * pregunta si existe una organization bajo el id introducido
	 * @param department entidad departamento
	 * @return response entity con cabeceras de mensajes, null en el body si no se pudo insertar el departamento
	 */
	public ResponseEntity<Department> save(Department department){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status;
		Department result = null;
		try {
			//consumir microservicio de organization
			ResponseEntity<Boolean> response = apiClient.organizationExist(department.getIdOrganization());
			if (response.getStatusCode().equals(HttpStatus.OK)) {
				//si la organization existe se guarda
				if(response.getBody()) {
					result = departmentRepository.saveAndFlush(department);
					status = HttpStatus.OK;
					headers.add(AppConst.MSG_DESCRIPTION, "guardado correctamente");
					log.info("departamento guardado correctamente");
				}else {
					//si no existe la organization entonces se manda un bad request
					//ademas de detalles del error
					status = HttpStatus.BAD_REQUEST;
					headers.add(AppConst.MSG_ERROR, "No existe organizacion con id "+department.getIdOrganization());
				}
			}else {
				//en caso que el estado no sea OK
				//se setea los headers de la consulta a esta respuesta
				headers.addAll(response.getHeaders());
				status = response.getStatusCode();
			}
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, "Ocurrio un error al intentar guardar departamento");
		}
		return new ResponseEntity<>(result,headers,status);
	}
	
	/**
	 * funcion de busqueda de departamento
	 * busca un departamento dado el id ingresado
	 * maneja posibles resultados de: encontrado, no encontrado y exceptions
	 * @param id departamento
	 * @return response entity con cabeceras de mensajes, null en el body si no se encontro el departamento
	 */
	public ResponseEntity<Department> findById(Long id){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status = HttpStatus.OK;//por defecto ok, solo cambia si entra en error
		Department result = null;
		try {
			//buscar el departamento, esto se envuelve en Optional
			Optional<Department> searched = departmentRepository.findById(id);
			//si el departamento esta presente
			if(searched.isPresent()) {
				result = searched.get();//obtengo el departamento
				headers.add(AppConst.MSG_DESCRIPTION, "encontrado correctamente");
				log.info("departamento encontrado");
			}else {
				headers.add(AppConst.MSG_DESCRIPTION, "No se encontro departamento con id "+id);
			}
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, "Ocurrio un error al intentar buscar departamentos con id "+id);
		}
		return new ResponseEntity<>(result,headers,status);
	}
	
	/**
	 * funcion de busqueda de todos los departamentos
	 * evalua si la lista esta vacia y tambien si ocurre algun error lo maneja con try-catch
	 * @return response entity con cabeceras de mensajes, null en el body si ocurrio alguna exception mientras se buscaba
	 */
	public ResponseEntity<List<Department>> findAll(){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status;
		List<Department> result = null;
		//try catch para manejar exceptions que se puedan producir
		try {
			result = departmentRepository.findAll();
			status = HttpStatus.OK;
			headers.add(AppConst.MSG_DESCRIPTION,"Departamentos encontrados : "+result.size());
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, "Ocurrio un error al intentar buscar departamento");
		}
		return new ResponseEntity<>(result,headers,status);
	}
	
	/**
	 * funcion de busqueda de todos los departamentos
	 * bajo el id de organization enviado
	 * evalua si la lista esta vacia y tambien si ocurre algun error lo maneja con try-catch
	 * @param id organization
	 * @return response entity con cabeceras de mensajes, null en el body si ocurrio alguna exception mientras se buscaba
	 */
	public ResponseEntity<List<Department>> findAllByIdOrganization(Long id){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status;
		List<Department> result = null;
		try {
			result = departmentRepository.findAllByIdOrganization(id);
			status = HttpStatus.OK;
			headers.add(AppConst.MSG_DESCRIPTION,"Departamentos encontrados : "+result.size());
			/*
			//si la lista esta vacia
			if(result.isEmpty()) {
				status = HttpStatus.BAD_REQUEST;
				headers.add(AppConst.MSG_ERROR,"No se encontraron departamentos con el id de organization: "+id);
			}else {//si la lista no esta vacia
				status = HttpStatus.OK;
				headers.add(AppConst.MSG_DESCRIPTION,"Deparamentos encontrados : "+result.size());
			}
			*/
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, "Ocurrio un error al intentar buscar departamento");
		}
		return new ResponseEntity<>(result,headers,status);
	}
	
	/**
	 * funcion de busqueda de departamentos bajo id de organization
	 * ademas de llenado de empleados bajo el id de departamento
	 * consulta en una iteracion al microservicio de employee para cada departamento
	 * que pertenezca a la organization enviada
	 * @param id organization
	 * @return response entity con cabeceras de mensajes, null en el body si no se realizo los procesos correctament
	 */
	public ResponseEntity<List<Department>> findAllByIdOrganizationWithEmployees(Long id){
		//utilizo funcion de encontrar todos los departamentos bajo el id
		//esta funcion es local no se hace llamados a microservicios
		ResponseEntity<List<Department>> response = findAllByIdOrganization(id);
		//obtengo valores de cabecera
		HttpStatus status 	= response.getStatusCode();
		HttpHeaders headers = new HttpHeaders();
		headers.addAll(response.getHeaders());
		//creo la lista de departamento e inicialmente le pongo valor nulo
		List<Department> departments=null;
		//si la respuesta es ok es por que hay departamentos bajo el id de organization
		if(status.equals(HttpStatus.OK)) {
			//llenar la lista
			departments = response.getBody();
			//para cada departamento en la lista de departamentos
			if (departments.isEmpty()) {
				headers.add(AppConst.MSG_DESCRIPTION,"no hay departamentos para la organizacion con id: "+id);
			}
			for (Department department : departments) {
				//obtener respuesta a la llamada al microservicio de employee
				ResponseEntity<List<Employee>> responseEmployee = apiClient.findEmployeesByIdDepartment(department.getId());
				//si la respuesta es ok
				if(responseEmployee.getStatusCode().equals(HttpStatus.OK)) {
					//lleno el departamento con los empleados encontrados
					department.setEmployees(responseEmployee.getBody());
				}else {
					//si ocurrio algun error cuando se consultaba el servicio employee
					headers.addAll(responseEmployee.getHeaders());
					status = HttpStatus.GATEWAY_TIMEOUT; //no se pudo conectar al microservicio de employee
					break; // se detiene y no sigue realizando peticiones - sale del bucle
				}
			}
		}else {
			headers.addAll(headers);
		}
		return new ResponseEntity<>(departments,headers,status);
	}
}
